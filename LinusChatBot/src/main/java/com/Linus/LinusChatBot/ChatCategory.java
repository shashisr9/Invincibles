package com.Linus.LinusChatBot;

import org.springframework.stereotype.Component;

@Component
public class ChatCategory {

	private String categoryID;
	private String CategoryName;
	public String getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(String categoryID) {
		this.categoryID = categoryID;
	}
	public String getCategoryName() {
		return CategoryName;
	}
	public void setCategoryName(String categoryName) {
		CategoryName = categoryName;
	}
	
}
